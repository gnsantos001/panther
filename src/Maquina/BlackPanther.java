package Maquina;


	import robocode.*;
	import robocode.HitRobotEvent;
	import robocode.ScannedRobotEvent;

	import java.awt.*;


	public class BlackPanther extends AdvancedRobot {

		boolean peek;
		double moveAmount; 


		public void run() {
		
			setBodyColor(Color.black);
			setGunColor(Color.black);
			setRadarColor(Color.black);
			setBulletColor(Color.black);
			setScanColor(Color.black);

			
			moveAmount = Math.max(getBattleFieldWidth(), getBattleFieldHeight());
			
			peek = false;

		
			turnLeft(getHeading());
			
			ahead(moveAmount);
			
			peek = true;
			
			setAhead(100);
			setTurnRight(90);
			
			//curva para direita
			
			execute();
			
			//curva para direita
			
			ahead(100);   // Para frente
			back(90);     // Para tr�s
			execute();
			
			while (true) {
			
				peek = true;
				
				setAhead(moveAmount);
				setTurnRight(90);
				
				//curva para direita
				
			
				peek = false;
				
				
				setAhead(100);
				setTurnLeft(90);
				
				// Curva para esquerda
				
				execute();
			}
		}


		public void onHitRobot(HitRobotEvent e) {
		
			if (e.getBearing() > -150 && e.getBearing() < 150) {
				
				setBack(100);
				setTurnLeft(90);
				
				// Curva para tr�s a esquerda
				
				execute();
				
					
				
			} 
			else {
				
				setBack(100);
				setTurnRight(100);
				
				// Curva para tr�s a esquerda
			}
		}


		public void onScannedRobot(ScannedRobotEvent e) {
				setAhead(100);
				fire(2);       // Fogo!
				setAhead(100);
				
			
		
			if (peek) {
				scan();
			}
		}
	}


